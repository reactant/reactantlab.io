import React, { useState } from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.scss';

function Home(props) {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  const [isPlayStartAnimation, setIsPlayStartAnimation] = useState(false)

  const randomBubbles = new Array(60).fill(null).map((_, index) => ({
    key: index,
    left: Math.random() * 110 - 5,
    top: Math.random() * 400 + 60,
    size: Math.random() * 8 + 2
  }))

  const triggered = false;

  const introductionUrl = useBaseUrl('docs/core/introduction')
  const onDelayLinkClick = (e) => {
    e.preventDefault();
    if (!triggered) {
      setIsPlayStartAnimation(true)
      setTimeout(() => props.history.push(introductionUrl), 1800);
    }
  }
  return (
    <Layout
      description="Reactant documentation">
      <div className={styles.landing}>
        <div className={classnames('hero hero--primary', styles.heroBanner)}>
          <div className={classnames('container', styles.mainContent)}>
            <h1 className="hero__title">{siteConfig.title}</h1>
            <p className="hero__subtitle">{siteConfig.tagline}</p>
            <div className={styles.buttons}>
              <Link
                className={classnames(
                  'button button--outline button--secondary button--lg',
                )}
                onClick={onDelayLinkClick}
                to={useBaseUrl('docs/core/introduction')}>
                Get Started
              </Link>
            </div>
          </div>
        </div>
        <div className={classnames(styles.liquid, isPlayStartAnimation ? styles.startTravel : null)}>
          {
            randomBubbles.map(bubble =>
              <div
                key={bubble.key}
                className={classnames(styles.bubble)}
                style={{
                  left: `${bubble.left}vw`,
                  top: `${bubble.top}vh`,
                  width: `${bubble.size}vh`,
                  height: `${bubble.size}vh`
                }} />
            )
          }
        </div>
        <div className={styles.rippleLeft} />
        <div className={styles.rippleRight} />
      </div>
    </Layout>
  );
}

export default Home;
