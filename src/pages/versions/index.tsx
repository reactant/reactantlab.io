import React, { useEffect, useState } from 'react';
import classnames from 'classnames';
// @ts-ignore
import Layout from '@theme/Layout';
import { fromFetch } from "rxjs/fetch";
import { catchError, flatMap, map } from "rxjs/operators";
import styles from './index.module.scss';
import { faDownload, faFileCode } from "@fortawesome/free-solid-svg-icons";
import { Observable, of } from "rxjs";
import { Box, FormControl, IconButton, InputLabel, LinearProgress, MenuItem, Popover, Select } from "@material-ui/core";
import PopupState, { bindPopover, bindTrigger } from 'material-ui-popup-state';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

interface ArtifactsResult {
  name: string;
  versions: {
    [name: string]: {
      suffix?: string,
      downloadUrl?: string
    }
  };
}

function fetchRepository(repositoryUrl: string, name: string, isSnapshot: boolean): Observable<ArtifactsResult> {
  const domParser = new DOMParser();
  const byPassProxy = "https://cors-anywhere.herokuapp.com"
  const corsUrl = `${byPassProxy}/${repositoryUrl}/${name}`

  const requestInit = { method: "GET", headers: [["Origin", "null"], ["cache-control", "public, max-age=300"]] }

  return fromFetch(`${corsUrl}/maven-metadata.xml`, requestInit).pipe(
    flatMap((resp: Response) => resp.text()),
    map((body: string) => domParser.parseFromString(body, "text/xml")),
    map((xmlBody: Document) => {
      const result: ArtifactsResult = { name, versions: {} }
      if (isSnapshot) {
        result.versions[xmlBody.getElementsByTagName("latest")[0].textContent] = {}
      } else {
        const versionEls = xmlBody.getElementsByTagName("version")
        for (let i = 0; i < versionEls.length; i++) {
          const version = versionEls[i].textContent
          result.versions[version] = {
            downloadUrl: `${repositoryUrl}/${name}/${version}/${name}-${version}-all.jar`
          }
        }
      }
      return result;
    }),
    flatMap((result: ArtifactsResult) => {
      if (isSnapshot) {
        const version = Object.keys(result.versions)[0]
        return fromFetch(`${corsUrl}/${version}/maven-metadata.xml`).pipe(
          flatMap((resp: Response) => resp.text()),
          map((body: string) => domParser.parseFromString(body, "text/xml")),
          map((xmlBody: Document) => {
            const snapshotNumber = xmlBody.getElementsByTagName("snapshotVersion")[0].getElementsByTagName("value")[0].textContent;
            result.versions[version].suffix = snapshotNumber;
            result.versions[version].downloadUrl = `${repositoryUrl}/${name}/${version}/${name}-${snapshotNumber}-all.jar`;
            return result;
          })
        )
      }
      return of(result)
    }),
    catchError((err) => of({ name, versions: {} }))
  )
}

function GradleInfo({ artifacts, version }) {
  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => (
        <div>
          <IconButton color="primary" aria-label="upload picture"
                      component="span"{...bindTrigger(popupState)}>
            <FontAwesomeIcon icon={faFileCode} />
          </IconButton>
          <Popover
            {...bindPopover(popupState)}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          >
            <Box p={2}> compileOnly("dev.reactant:{artifacts.name}:{version}") </Box>
          </Popover>
        </div>
      )}
    </PopupState>
  )
}

function Version(props) {

  const snapshotsUrl = `https://oss.sonatype.org/content/repositories/snapshots/dev/reactant`
  const releasesUrl = `https://oss.sonatype.org/content/repositories/releases/dev/reactant`


  const [targetItem, setTargetItem] = useState("reactant")
  const [snapshots, setSnapshots] = useState<ArtifactsResult>(null)
  const [releases, setReleases] = useState<ArtifactsResult>(null)


  useEffect(() => {
    setSnapshots(null);
    setReleases(null);
    const snapshotSubscription = fetchRepository(snapshotsUrl, targetItem, true).subscribe((result) => {
      setSnapshots(result);
    })
    const releaseSubscription = fetchRepository(releasesUrl, targetItem, false).subscribe((result) => setReleases(result))
    return () => {
      snapshotSubscription.unsubscribe();
      releaseSubscription.unsubscribe();
    }
  }, [targetItem])


  return (
    <Layout
      title={`Versions`}
      description="Reactant versions">
      <div className={classnames("container", "margin-vert--lg")}>
        <div className={styles.titleRow}>
          <h1>Versions</h1>
          <FormControl>
            <InputLabel htmlFor="select-project">Project build</InputLabel>
            <Select
              id="select-project"
              value={targetItem}
              onChange={(e) => setTargetItem(e.target.value as string)}
            >
              <MenuItem value={"reactant"}>Reactant Core</MenuItem>
              <MenuItem value={"resource-stirrer"}>Resource Stirrer</MenuItem>
              <MenuItem value={"enriched-ui"}>Enriched UI</MenuItem>
              <MenuItem value={"mechanism"}>Mechanism</MenuItem>
            </Select>
          </FormControl>
        </div>


        {(!snapshots || !releases) && <LinearProgress />}

        <div className={styles.versionsContainer}>
          <h2>Latest Snapshot</h2>
          {snapshots == null}
          {
            snapshots && Object.entries(snapshots.versions).map(([version, snapshot]) => (
              <div key={version} className={classnames(styles.versionRow)}>
                <div>
                  <div>{version}</div>
                  <div>Build: {snapshot.suffix}</div>
                </div>
                <div className={styles.operations}>
                  <GradleInfo artifacts={snapshots} version={version} />
                  <a className={classnames(styles.downloadBtn)}
                     href={snapshot.downloadUrl}
                     target="_blank">
                    <IconButton color="primary" aria-label="upload picture" component="span">
                      <FontAwesomeIcon icon={faDownload} />
                    </IconButton>
                  </a>
                </div>
              </div>
            ))
          }
        </div>

        <div className={styles.versionsContainer}>
          <h2>Releases</h2>
          {
            releases && Object.entries(releases.versions).reverse().map(([version, release]) => (
              <div key={version} className={classnames(styles.versionRow)}>
                <div>
                  <div>{version}</div>
                </div>
                <div className={styles.operations}>
                  <GradleInfo artifacts={releases} version={version} />
                  <a className={classnames(styles.downloadBtn)}
                     href={release.downloadUrl}
                     target="_blank">
                    <IconButton color="primary" aria-label="upload picture" component="span">
                      <FontAwesomeIcon icon={faDownload} />
                    </IconButton>
                  </a>
                </div>
              </div>
            ))
          }
        </div>
      </div>
    </Layout>
  )
}


export default Version;
