import { createMuiTheme } from "@material-ui/core";

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#8d95b9",
    },
    secondary: {
      main: "#7092a5",
    },
  },
});
