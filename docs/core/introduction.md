---
title: Introduction
sidebar_label: Introduction
---

## What is reactant?

Reactant is a spigot plugin framework for developer to build their plugin faster and correctly.
Reactant core provided a better way for interaction between "services" and plugins by using dependency injection.
In addition, Reactant will provide tools that useful for developer to develop the spigot plugins in a elegant way.

## How should I start?

In this document, we will use [Kotlin](https://kotlinlang.org/docs/reference/) as the code example language.
If you come from Java, ensure you have the basic knowledge of Kotlin, although they are similar.

In addition, reactant use [ReactiveX](http://reactivex.io/) in most of the features, please make sure you have the basic knowledge on it.

:::note Can I use Java?
It is not suggested that you use Reactant with Java,
you may get stuck on some problem such as [inline functions](https://kotlinlang.org/docs/reference/inline-functions.html) and [extension functions](https://kotlinlang.org/docs/reference/extensions.html).
In addition, you won't be available to enjoy Kotlin's elegant DSL.
:::

You are not required to be a Bukkit API master to use Reactant, 
let's start our adventure when you are ready. 



## Contribute

If you are interest on [reactant](https://gitlab.com/reactant/reactant), please help us improve.

