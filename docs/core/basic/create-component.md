---
title: Create Component
sidebar_label: Create Component
---

## Reactant Component

Reactant component is the most basic unit that will be loaded by Reactant framework. 
A component can be any module, the following is some examples:
 - Player Data Holder
 - Economy Service
 - Customer Item Manager
 - GUI Generator
 - Fish Some TNT Controller
 - Whatever you want to do... 

### `@Component`
There have different way to provide a Component in Reactant, 
let's start with the easiest `@Component` Annotation.

To create a component class, just simply add an annotation `@Component` to the class.
The annotation`@Component` is used to declare that it is a singleton component.
```kotlin
@Component
class HelloWorldComponent {

    fun sayHello(){
        MyFirstPlugin.log.info("Hello World!")
    }

}
```

You have already created your first component! Wait, this class is doing nothing...

### Component Life Cycle Hook
At this moment, your component won't have any effect, 
it is because you haven't specified what should the component do when enabling.

To make your component call `sayHello()` when enable, make `HelloWorldComponent` implements `LifeCycleHook` interface.
```kotlin
@Component
class HelloWorldComponent : LifeCycleHook {

    override fun onEnable(){
        sayHello()
    }

    fun sayHello(){
        MyFirstPlugin.log.info("Hello World!")
    }

}
```

Now you can compile your plugin again, and you will see the "Hello World!" info log in your console, cheers!
