---
title: Inject Object
sidebar_label: Inject Object
---

## Basic Object Injection
Now you know how to create a component, but what if we would like to use other components in a component? 
For this case, you can declare the components you need as the class primary constructor parameters.

```kotlin
@Component
class HelloWorldMessageProvider {
    val message = "Hello World!"
}

@Component
class SayHelloToConsole(
    private val messageProvider : HelloWorldMessageProvider
) : LifeCycleHook {

    override fun onEnable(){
        MyFirstPlugin.log.info(messageProvider.message)
    }

}
```

Reactant will automatically arrange the load order of your components, and inject the required objects when constructing.

## Abstraction
Instead of inject the object by the exact class, you can also inject the object by abstract class or interface.


```kotlin
interface MessageProvider{
    val message
}

@Component
class HelloWorldMessageProvider : MessageProvider {
    override val message = "Hello World!"
}

@Component
class SayHelloToConsole(
    private val messageProvider : MessageProvider
) : LifeCycleHook {

__    override fun onEnable(){
        MyFirstPlugin.log.info(messageProvider.message)
    }

}
```
