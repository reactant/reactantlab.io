---
title: i18n
sidebar_label: i18n
---

Reactant translation implemented with reflections, 
which allow you to get the translated text with a neat function call.

## Define your translation string table
The `I18nTable` is a class which act like a schema, the table class must:
 1. Annotated with `@I18n()`
 2. Implemented I18nTable
 3. open concrete class (i.e. open non-abstract)
 4. No constructor parameters

To define your translation string, you can declare an `open` function with the parameters you need and return the default translation inside the `I18nTable`.

```kotlin
import dev.reactant.reactant.extra.i18n.I18n
import dev.reactant.reactant.extra.i18n.I18nTable

@I18n("This is an example translation table")
open class ExampleI18nTable : I18nTable {

    open fun blamePlayer(playerName: String, itemName: String) = "$playerName destroyed the $itemName! ;C"

    open fun summon(playerName: String, petType: String, amount: Int) = "$playerName summoned $amount $petType!"

}
```
## Edit translation
### Generate translation file
We provided a command tools for you to manage your translations file more easily.

To list all declared i18n table in the server, run the following command:
```bash
reactant i18n ls
```
To generate the dummy translation file for further usage, run the following command:
```bash
# example to generate en and zh_HK language file for all i18n table
reactant i18n generate --language en --language zh_HK

# example to generate en language file for all i18n table start with com.example.
reactant i18n generate com.example.*
```

Now you can go to `plugins/Reactant/i18n/tables/<path_to_your_class>/en.json` to find your translation file.
The generated translation file contains the parameters name to hint you.
```json
{
  "translations": {
    "blamePlayer": "$playerName $itemName",
    "summon": "$playerName $petType $amount"
  }
}
```
You can edit the text in the translation file and use `$PARAMETER_NAME` to replace the text. 
The missing translation field will be replaced with the default translation from the i18n table.
```json
{
  "translations": {
    "blamePlayer": "$playerName destroyed my lovely $itemName..."
  }
}
```

## Use the translation string
### Injectable
To get the translation string in component, you can inject the translation class directly.
```kotlin
@Component
class ExampleTranslationUsage(
        private val exampleTexts: ExampleI18nTable
) : LifeCycleHook{

    override fun onEnable() {
        MyFirstPlugin.log.info(exampleTexts.blamePlayer("The rats", "keyboard"))
    }

}
```

### Change server default language
You can change the server default language setting in `plugins/Reactant/i18n/config.json`:

```json
{
  "languages": [
    "zh_HK",
    "en"
  ]
}
```
