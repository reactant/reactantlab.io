module.exports = {
  title: 'Reactant',
  tagline: 'An elegant plugin framework for spigot',
  url: 'https://reactant.dev',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  organizationName: 'Reactant Dev', // Usually your GitHub org/user name.
  projectName: 'Reactant', // Usually your repo name.
  themeConfig: {
    colorMode: {
      defaultMode: 'light',
      disableSwitch: false,
      respectPrefersColorScheme: true,
    },
    navbar: {
      title: 'Reactant',
      logo: {
        alt: 'Reactant Logo',
        src: 'img/logo.png',
      },
      items: [
        {
          to: 'docs/core/introduction',
          activeBasePath: 'docs',
          label: 'Docs', position: 'left',
        },
        // {to: 'blog', label: 'Blog', position: 'left'},
        {to: 'versions', label: 'Versions', position: 'right'},
        {href: 'https://gitlab.com/reactant/reactant', label: 'GitLab', position: 'right'},
      ],
      hideOnScroll: true,
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Discord',
              href: 'https://discord.gg/9NufxVr',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/reactant',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Reactant Dev`,
    },
    prism: {
      theme: require('prism-react-renderer/themes/nightOwlLight'),
      darkTheme: require('prism-react-renderer/themes/nightOwl'),
      additionalLanguages: ['kotlin'],
    },
    gtag: {
      trackingID: 'UA-171022552-1',
      anonymizeIP: true,
    },
    algolia: {
      apiKey: '5197a610e2d931a65575ec4417f9c5ec',
      indexName: 'reactant',
      searchParameters: {}, // Optional (if provided by Algolia)
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/reactant/reactant.gitlab.io/-/edit/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.scss'),
        },
      },
    ],
  ],
  plugins: [
    'docusaurus-plugin-sass',
  ],
};
