/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

module.exports = {
  someSidebar: {
    "Reactant Core": [
      'core/introduction',
      {
        type: 'category',
        label: 'Basic Tutorial',
        items: [
          'core/basic/create-project',
          'core/basic/create-component',
          'core/basic/inject-object',
          'core/basic/event-listener',
          'core/basic/command',
          'core/basic/config',
          'core/basic/scheduler',
          'core/basic/i18n',
          'core/basic/http-client',
        ]
      },
      {
        type: 'category',
        label: 'Tricks',
        items: []
      },
      {
        type: 'category',
        label: 'In-depth Tutorial',
        items: []
      }
    ],
  },
};
